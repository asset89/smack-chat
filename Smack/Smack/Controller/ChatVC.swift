//
//  ViewController.swift
//  Smack
//
//  Created by МакЛарен on 03.09.2018.
//  Copyright © 2018 Asset Ryskul. All rights reserved.
//

import UIKit

class ChatVC: UIViewController {
    
    
    //MARK: - Outlet
    @IBOutlet weak var menuBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //MARK: - Slide ChatVC above ChannelVC help with SWReveal tapping button
        menuBtn.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        //back slide tapping any place in screen
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
    }

   

}

