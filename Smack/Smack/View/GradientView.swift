//
//  GradientView.swift
//  Smack
//
//  Created by МакЛарен on 04.09.2018.
//  Copyright © 2018 Asset Ryskul. All rights reserved.
//

import UIKit

@IBDesignable
class GradientView: UIView {
    
    //MARK: - Создание фонового цвета с меняищимся градиентом
    
    @IBInspectable var topColor: UIColor = #colorLiteral(red: 0.2901960784, green: 0.3019607843, blue: 0.8470588235, alpha: 1) {  //change in StoryBoard dynamically
        didSet {
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable var bottomColor: UIColor = #colorLiteral(red: 0.1725490196, green: 0.831372549, blue: 0.8470588235, alpha: 1) {
        didSet {
            self.setNeedsLayout()
        }
    }
    
    //MARK: - непосредственно создаем фон с помощью метода
    override func layoutSubviews() {
        let gradientLayer = CAGradientLayer() //слой для рисования цвета в фоне
        gradientLayer.colors = [topColor.cgColor, bottomColor.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 1)
        gradientLayer.frame = self.bounds
        self.layer.insertSublayer(gradientLayer, at: 0)
        
    }
 

}
